/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.shapeproject;

/**
 *
 * @author ASUS
 */
public class Rectangle {
    private double wide;
    public double Long;
    public Rectangle(double wide, double Long){
        this.wide = wide;
        this.Long = Long;
    }
    public double calArea(){
        return wide * Long;
    }
    public double getW(){
        return wide;
    }
    public double getL(){
        return Long;
    }
    public void setWL(double wide,double Long){
        if(wide <= 0 || Long <= 0){
            System.out.println("Error: Wide and Long must more than zero!!!!");
            return;
        }
        this.wide = wide;
        this.Long = Long;
    }
}
