/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.shapeproject;

/**
 *
 * @author ASUS
 */
public class Square {
    private double side;
    public Square(double side){
        this.side = side;
    }
    public double calArea(){
        return side * side;
    }
    public double getS(){
        return side;
    }
    public void setS(double side){
        if(side <= 0){
            System.out.println("Error: Side must more than zero!!!!");
            return;
        }
        this.side = side;
    }
}
