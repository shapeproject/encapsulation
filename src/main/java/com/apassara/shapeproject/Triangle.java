/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.shapeproject;

/**
 *
 * @author ASUS
 */
public class Triangle {
    private double base;
    public double high;
    public static final double T = 1.0/2;
    public Triangle(double base,double high){
        this.base = base;
        this.high = high;
    }
    public double calArea(){
        return T * base * high;
    }
    public double getB(){
        return base;
    }
    public double getH(){
        return high;
    }
    public void setBH(double base, double high){
        if(base <= 0 || high <= 0){
            System.out.println("Error: Base and High must more than zero!!!!");
            return;
        }
        this.base = base;
        this.high = high;
    }
    }
    
     
